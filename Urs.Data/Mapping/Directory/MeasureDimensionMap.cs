
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Directory;

namespace Urs.Data.Mapping.Directory
{
    public partial class MeasureDimensionMap : UrsEntityTypeConfiguration<MeasureDimension>
    {
        public override void Configure(EntityTypeBuilder<MeasureDimension> builder)
        {
            builder.ToTable(nameof(MeasureDimension));
            builder.HasKey(m => m.Id);
            builder.Property(m => m.Name).IsRequired().HasMaxLength(100);
            builder.Property(m => m.SystemKeyword).IsRequired().HasMaxLength(100);
            builder.Property(m => m.Ratio).HasColumnType("decimal(18, 8)");
            base.Configure(builder);
        }
    }
}