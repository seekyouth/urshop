
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Orders;

namespace Urs.Data.Mapping.Orders
{
    public partial class AfterSalesMap : UrsEntityTypeConfiguration<AfterSales>
    {
        public override void Configure(EntityTypeBuilder<AfterSales> builder)
        {
            builder.ToTable(nameof(AfterSales));
            builder.HasKey(rr => rr.Id);
            builder.Property(rr => rr.ReasonForReturn).IsRequired();
            builder.Property(rr => rr.RequestedAction).IsRequired();
            builder.Property(rr => rr.UserComments);
            builder.Property(rr => rr.StaffNotes);
            builder.Property(rr => rr.Images);

            builder.Ignore(rr => rr.AfterSalesStatus);
            
            base.Configure(builder);
        }
    }
}