﻿using System;
using Urs.Core;

namespace Urs.Data.Domain.Coupons
{
    public partial class Coupon:BaseEntity
    {
        public virtual decimal Value { get; set; }
        public virtual DateTime StartTime { get; set; }
        public virtual DateTime EndTime { get; set; }
        public virtual decimal MinimumConsumption { get; set; }
        public virtual string Title { get; set; }
        public virtual int Amount { get; set; }
        public virtual int UsedAmount { get; set; }
        public virtual bool IsAmountLimit { get; set; }
    }
}
