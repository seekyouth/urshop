﻿using System.Collections.Generic;
using Urs.Core.Configuration;

namespace Urs.Data.Domain.Configuration
{
    public class ShippingSettings : ISettings
    {
        public ShippingSettings()
        {
            ActiveShippingRateComputationMethodSystemNames = new List<string>();
        }

        public bool Enabled { get; set; }
        /// <summary>
        /// Gets or sets an system names of active shipping rate computation methods
        /// </summary>
        public List<string> ActiveShippingRateComputationMethodSystemNames { get; set; }
        public List<string> ActiveShippingExpressNames { get; set; }
        public int TimeDeliverDay { get; set; }
        public int TimeConfirmOrderRange { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether 'Free shipping over X' is enabled
        /// </summary>
        public bool FreeShippingOverXEnabled { get; set; }
        /// <summary>
        /// Gets or sets a value of 'Free shipping over X' option
        /// </summary>
        public decimal FreeShippingOverXValue { get; set; }
        /// <summary>
        /// A value indicating whether users should see shipment events on their order details pages
        /// </summary>
        public bool DisplayShipmentEventsToUsers { get; set; }

        /// <summary>
        /// Gets or sets shipping origin address
        /// </summary>
        public int ShippingOriginAddressId { get; set; }
        public List<string> ActivePickupPointProviderSystemNames { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether we should return valid options if there are any (no matter of the errors returned by other shipping rate compuation methods).
        /// </summary>
        public bool ReturnValidOptionsIfThereAreAny { get; set; }

    }
}