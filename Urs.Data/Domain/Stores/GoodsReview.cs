using Urs.Core;
using System;

namespace Urs.Data.Domain.Stores
{
    /// <summary>
    /// 商品评论
    /// </summary>
    public partial class GoodsReview : BaseEntity
    {
        /// <summary>
        /// 产品Id
        /// </summary>
        public virtual int GoodsId { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public virtual int UserId { get; set; }
        /// <summary>
        /// 订单Id
        /// </summary>
        public virtual int OrderId { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public virtual string Title { get; set; }
        /// <summary>
        /// 评价内容
        /// </summary>
        public virtual string ReviewText { get; set; }

        /// <summary>
        /// Review rating
        /// </summary>
        public virtual int Rating { get; set; }
        /// <summary>
        /// IP地址
        /// </summary>
        public virtual string IpAddress { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public virtual DateTime CreateTime { get; set; }
    }
}
