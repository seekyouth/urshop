using Urs.Core;
namespace Urs.Data.Domain.Users
{
    /// <summary>
    /// 外部授权
    /// </summary>
    public partial class ExternalAuth : BaseEntity
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public virtual int UserId { get; set; }
        /// <summary>
        /// 微信名
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public virtual string AvatarUrl { get; set; }

        /// <summary>
        /// OpenId
        /// </summary>
        public virtual string OpenId { get; set; }
        /// <summary>
        /// Token
        /// </summary>
        public virtual string OAuthToken { get; set; }

        /// <summary>
        /// 刷新Token
        /// </summary>
        public virtual string OAuthRefreshToken { get; set; }
        /// <summary>
        /// UnionId
        /// </summary>
        public virtual string UnionId { get; set; }
        /// <summary>
        /// Gets or sets the provider
        /// </summary>
        public virtual string ProviderSystemName { get; set; }

        /// <summary>
        /// Gets or sets the user
        /// </summary>
        public virtual User User { get; set; }
    }

}
