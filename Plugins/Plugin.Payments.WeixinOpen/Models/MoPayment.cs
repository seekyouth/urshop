﻿using System.Collections.Generic;
using Urs.Framework.Mvc;

namespace Urs.Plugin.Payments.WeixinOpen.Models
{
    public class MoPayment : BaseModel
    {
        public MoPayment()
        {
            KeyValue = new Dictionary<string, string>();
        }
        public int OrderId { get; set; }
        public string OrderPrice { get; set; }
        public string CreateTime { get; set; }

        public Dictionary<string, string> KeyValue { get; set; }
    }
}