using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Urs.Core;
using Urs.Data.Domain.Orders;
using Urs.Data.Domain.Payments;
using Urs.Core.Plugins;
using Urs.Data.Domain.Configuration;
using Urs.Services.Configuration;
using Urs.Services.Users;
using Urs.Services.Localization;
using Urs.Services.Logging;
using Urs.Services.Payments;
using Urs.Services.Tasks;

namespace Urs.Plugin.Payments.WeixinOpen
{
    public class WeixinOpenPaymentMethod : BasePlugin, IPaymentMethod
    {
        #region Fields

        private readonly IUserService _userService;
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly ILocalizationService _localizationService;
        private readonly ILogger _logger;
        private readonly IPaymentService _paymentService;
        private readonly ISettingService _settingService;
        private readonly IScheduleTaskService _scheduleTaskService;
        private readonly IWebHelper _webHelper;
        private readonly WeixinOpenPaymentSettings _aliPayPaymentSettings;
        private readonly IHttpContextAccessor _httpContextAccessor;

        #endregion

        #region Ctor

        public WeixinOpenPaymentMethod(IUserService userService,
            StoreInformationSettings storeInformationSettings,
            ILocalizationService localizationService,
            ILogger logger,
            IPaymentService paymentService,
            ISettingService settingService,
            IScheduleTaskService scheduleTaskService,
            IWebHelper webHelper,
            WeixinOpenPaymentSettings squarePaymentSettings,
            IHttpContextAccessor httpContextAccessor)
        {
            this._userService = userService;
            this._storeInformationSettings = storeInformationSettings;
            this._localizationService = localizationService;
            this._logger = logger;
            this._paymentService = paymentService;
            this._settingService = settingService;
            this._scheduleTaskService = scheduleTaskService;
            this._webHelper = webHelper;
            this._aliPayPaymentSettings = squarePaymentSettings;
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Utilities

        #endregion

        #region Methods

        #region Utilities

        /// <summary>
        /// Gets MD5 hash
        /// </summary>
        /// <param name="Input">Input</param>
        /// <param name="Input_charset">Input charset</param>
        /// <returns>Result</returns>
        public string GetMD5(string Input, string Input_charset)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] t = md5.ComputeHash(Encoding.GetEncoding(Input_charset).GetBytes(Input));
            StringBuilder sb = new StringBuilder(32);
            for (int i = 0; i < t.Length; i++)
            {
                sb.Append(t[i].ToString("x").PadLeft(2, '0'));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Bubble sort
        /// </summary>
        /// <param name="Input">Input</param>
        /// <returns>Result</returns>
        public string[] BubbleSort(string[] Input)
        {
            int i, j;
            string temp;

            bool exchange;

            for (i = 0; i < Input.Length; i++)
            {
                exchange = false;

                for (j = Input.Length - 2; j >= i; j--)
                {
                    if (System.String.CompareOrdinal(Input[j + 1], Input[j]) < 0)
                    {
                        temp = Input[j + 1];
                        Input[j + 1] = Input[j];
                        Input[j] = temp;

                        exchange = true;
                    }
                }

                if (!exchange)
                {
                    break;
                }
            }
            return Input;
        }

        /// <summary>
        /// Create URL
        /// </summary>
        /// <param name="Para">Para</param>
        /// <param name="InputCharset">Input charset</param>
        /// <param name="Key">Key</param>
        /// <returns>Result</returns>
        public string CreatUrl(string[] Para, string InputCharset, string Key)
        {
            int i;
            string[] Sortedstr = BubbleSort(Para);
            StringBuilder prestr = new StringBuilder();

            for (i = 0; i < Sortedstr.Length; i++)
            {
                if (i == Sortedstr.Length - 1)
                {
                    prestr.Append(Sortedstr[i]);

                }
                else
                {
                    prestr.Append(Sortedstr[i] + "&");
                }

            }

            prestr.Append(Key);
            string sign = GetMD5(prestr.ToString(), InputCharset);
            return sign;
        }

        /// <summary>
        /// Gets HTTP
        /// </summary>
        /// <param name="StrUrl">Url</param>
        /// <param name="Timeout">Timeout</param>
        /// <returns>Result</returns>
        public string Get_Http(string StrUrl, string content, int Timeout)
        {
            string strResult = string.Empty;
            try
            {
                strResult = HttpHelper.HttpPost(StrUrl, content, contentType: "application/x-www-form-urlencoded", timeout: Timeout);
            }
            catch (Exception exc)
            {
                strResult = "Error: " + exc.Message;
            }
            return strResult;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            result.NewPaymentStatus = PaymentStatus.Pending;
            return result;
        }

        /// <summary>
        /// Post process payment (used by payment gateways that require redirecting to a third-party URL)
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            var parameters = new Dictionary<string, string>
            {
                { "orderId", postProcessPaymentRequest.Order.Id.ToString()},
                { "paytype", nameof(Order) },
                { "openId", postProcessPaymentRequest.User.ExternalAuthRecords.FirstOrDefault()?.OpenId }
            };
            var url = QueryHelpers.AddQueryString(WeixinOpenPaymentDefaults.PaymentWeixinOpenPayingUrl, parameters);
            _httpContextAccessor.HttpContext.Response.Redirect(url);
        }

        /// <summary>
        /// Gets additional handling fee
        /// </summary>
        /// <returns>Additional handling fee</returns>
        public decimal GetAdditionalHandlingFee()
        {
            return decimal.Zero;
        }

        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            var result = new CapturePaymentResult();
            result.AddError("Capture method not supported");
            return result;
        }

        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            var result = new RefundPaymentResult();
            result.AddError("Refund method not supported");
            return result;
        }

        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            var result = new VoidPaymentResult();
            result.AddError("Void method not supported");
            return result;
        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            result.AddError("Recurring payment not supported");
            return result;
        }


        /// <summary>
        /// Gets a value indicating whether users can complete a payment after order is placed but not completed (for redirection payment methods)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public bool CanRePostProcessPayment(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            //Weixin is the redirection payment method
            //It also validates whether order is also paid (after redirection) so users will not be able to pay twice

            //payment status should be Pending
            if (order.PaymentStatus != PaymentStatus.Pending)
                return false;

            //let's ensure that at least 1 minute passed after order is placed
            if ((DateTime.Now - order.CreateTime).TotalMinutes < 1)
                return false;

            return true;
        }

        public override string GetConfigurationPageUrl()
        {
            return $"/Admin/PaymentWeixinOpen/Configure";
        }

        public override void Install()
        {
            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.WeixinOpen.AppId", "公众号号AppID");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.WeixinOpen.AppId.hint", "微信公众号->基本配置->开发者ID");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.WeixinOpen.AppSecret", "公众帐号Secert");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.WeixinOpen.AppSecret.hint", "微信公众号->基本配置->开发者密码");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.WeixinOpen.Partner", "商户号");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.WeixinOpen.Partner.hint", "微信公众号->微信支付->商户号管理");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.WeixinOpen.AppKey", "API密钥");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.WeixinOpen.AppKey.hint", "微信支付->账户中心->API安全->设置API密钥");

            base.Install();
        }


        public override void Uninstall()
        {
            //locales
            this.DeletePluginLocaleResource("Plugins.Payments.WeixinOpen.AppId");
            this.DeletePluginLocaleResource("Plugins.Payments.WeixinOpen.AppId.hint");
            this.DeletePluginLocaleResource("Plugins.Payments.WeixinOpen.AppSecret");
            this.DeletePluginLocaleResource("Plugins.Payments.WeixinOpen.AppSecret.hint");
            this.DeletePluginLocaleResource("Plugins.Payments.WeixinOpen.Partner");
            this.DeletePluginLocaleResource("Plugins.Payments.WeixinOpen.Partner.hint");
            this.DeletePluginLocaleResource("Plugins.Payments.WeixinOpen.AppKey");
            this.DeletePluginLocaleResource("Plugins.Payments.WeixinOpen.AppKey.hint");

            base.Uninstall();
        }

        public decimal GetAdditionalHandlingFee(decimal subTotal)
        {
            return default(decimal);
        }

        #endregion

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether capture is supported
        /// </summary>
        public bool SupportCapture
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public PaymentMethodType PaymentMethodType
        {
            get
            {
                return PaymentMethodType.Redirection;
            }
        }
        #endregion
    }
}