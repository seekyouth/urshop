using Urs.Core;
using Urs.Core.Plugins;
using Urs.Services.Authentication.External;
using Urs.Services.Configuration;
using Urs.Services.Localization;
using Urs.Services.Logging;

namespace ExternalAuth.WeixinOpen
{
    /// <summary>
    /// Represents Weixin payment method
    /// </summary>
    public class WeixinOpenMethod : BasePlugin, IExternalAuthenticationMethod
    {
        #region Fields

        private readonly ILogger _logger;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;

        #endregion

        #region Ctor

        public WeixinOpenMethod(
            ILogger logger,
            ISettingService settingService,
            IWebHelper webHelper)
        {
            this._logger = logger;
            this._settingService = settingService;
            this._webHelper = webHelper;
        }

        #endregion

        #region Methods

        public override string GetConfigurationPageUrl()
        {
            return $"/Admin/ExternalAuthWeixinOpen/Configure";
        }


        public override void Install()
        {
            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.ExternalAuth.WeixinOpen.AppId", "���ں�AppID");
            this.AddOrUpdatePluginLocaleResource("Plugins.ExternalAuth.WeixinOpen.AppSecret", "���ں�Secert");

            base.Install();
        }

        public override void Uninstall()
        {
            //locales
            this.DeletePluginLocaleResource("Plugins.ExternalAuth.WeixinOpen.AppId");
            this.DeletePluginLocaleResource("Plugins.ExternalAuth.WeixinOpen.AppSecret");

            base.Uninstall();
        }

        #endregion


    }
}