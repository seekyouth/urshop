﻿using System.Collections.Generic;
using Urs.Core;
using Urs.Core.Plugins;
using Urs.Services.Common;
using Urs.Services.Configuration;
using Urs.Services.Localization;

namespace Urs.Plugin.Weixin.MiniSDK
{
    public class MiniSDKEventProcessor : BasePlugin, IMiscPlugin
    {
        #region Fields
        private readonly ILocalizationService _localizationService;
        private readonly WeixinMiniSDKSettings _expressMiniSDKSettings;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;

        #endregion

        #region Ctor

        public MiniSDKEventProcessor(
            ILocalizationService localizationService,
            WeixinMiniSDKSettings expressMiniSDKSettings,
            ISettingService settingService,
            IWebHelper webHelper)
        {
            this._localizationService = localizationService;
            this._expressMiniSDKSettings = expressMiniSDKSettings;
            this._settingService = settingService;
            this._webHelper = webHelper;
        }

        #endregion

        #region Methods


        /// <summary>
        /// 配置地址
        /// </summary>
        public override string GetConfigurationPageUrl()
        {
            return $"/Admin/WeixinMiniSDK/Configure";
        }

        public string GetWidgetViewComponentName(string widgetZone)
        {
            return "WidgetsMiniSDKEvent";
        }

        public IList<string> GetWidgetZones()
        {
            return new List<string>();
        }

        /// <summary>
        /// Install plugin
        /// </summary>
        public override void Install()
        {
            base.Install();
        }

        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            base.Uninstall();
        }

        #endregion

    }
}