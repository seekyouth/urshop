﻿namespace Plugin.Api.Models.Common
{
    /// <summary>
    /// 用户头像
    /// </summary>
    public partial class MoBase64
    {
        public string base64Str { get; set; }
        public int pictureSize { get; set; }
    }
}