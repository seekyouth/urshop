﻿using System.Collections.Generic;

namespace Plugin.Api.Models.Common
{
    public partial class SelectListItemModel
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public bool Selected { get; set; }
    }
    /// <summary>
    /// 键值对
    /// </summary>
    public partial class KeyValueStrModel
    {
        /// <summary>
        /// 键
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        public string Value { get; set; }
    }
    /// <summary>
    /// 键值对
    /// </summary> 
    public partial class KeyValueModel
    {
        /// <summary>
        /// 键
        /// </summary>
        public int Key { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        public string Value { get; set; }
    }

    public partial class KeyValueArray
    {
        public KeyValueArray()
        {
            Items = new List<KeyValueArray>();
        }
        public string Key { get; set; }
        public string Value { get; set; }
        public IList<KeyValueArray> Items { get; set; }
    }
}