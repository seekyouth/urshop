﻿using System;

namespace Plugin.Api.Models.News
{
    /// <summary>
    /// 文章项
    /// </summary>
    public partial class MoNews
    {
        /// <summary>
        /// 文章Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 系统名称
        /// </summary>
        public string SeName { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        public string Short { get; set; }
        /// <summary>
        /// 全文内容
        /// </summary>
        public string Full { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}