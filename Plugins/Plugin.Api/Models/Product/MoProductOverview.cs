﻿using System.Collections.Generic;
using Plugin.Api.Models.Media;
using Plugin.Api.Models.Catalog;

namespace Plugin.Api.Models.Goods
{
    public partial class MoGoodsOverview
    {
        public MoGoodsOverview()
        {
            Owner = new MoOwner();
            Price = new MoPrice();
            Picture = new MoPicture();
            Fields = new List<MoCustomField>();
            Specs = new List<MoSpecification>();
            Tags = new List<MoGoodsTag>();
        }
        /// <summary>
        /// 商品Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 商品Sku
        /// </summary>
        public string Sku { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 子名称
        /// </summary>
        public string SubName { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        public string Short { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Full { get; set; }
        /// <summary>
        /// 商品url
        /// </summary>
        public string SeName { get; set; }
        /// <summary>
        /// 商品库存
        /// </summary>
        public int Qty { get; set; }
        /// <summary>
        /// 商品销量
        /// </summary>
        public int SaleVolume { get; set; }
        /// <summary>
        /// 商品上架
        /// </summary>
        public bool Published { get; set; }
        /// <summary>
        /// 商品价格
        /// </summary>
        public MoPrice Price { get; set; }
        /// <summary>
        /// 默认图片
        /// </summary>
        public MoPicture Picture { get; set; }
        /// <summary>
        /// 商品参数
        /// </summary>
        public IList<MoSpecification> Specs { get; set; }
        /// <summary>
        /// 商品标签
        /// </summary>
        public IList<MoGoodsTag> Tags { get; set; }

        /// <summary>
        /// 拥有者
        /// </summary>
        public MoOwner Owner { get; set; }
        /// <summary>
        /// 自定义字段
        /// </summary>
        public IList<MoCustomField> Fields { get; set; }

        public partial class MoCustomField
        {
            /// <summary>
            /// Id
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// 名称
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// 默认值
            /// </summary>
            public string DefaultValue { get; set; }
        }

        #region Nested Classes
        public partial class MoGoodsTag
        {
            /// <summary>
            /// 标签编号
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// 标签名称
            /// </summary>
            public string Name { get; set; }
        }
        /// <summary>
        /// 价格
        /// </summary>
        public partial class MoPrice
        {
            /// <summary>
            /// 旧价格、市场价
            /// </summary>
            public string OldPrice { get; set; }
            /// <summary>
            /// 销售价格、店铺价
            /// </summary>
            public string Price { get; set; }
            /// <summary>
            /// 成本价
            /// </summary>
            public string ProductCost { get; set; }
        }
        /// <summary>
        /// 拥有人，操作人
        /// </summary>
        public partial class MoOwner
        {
            /// <summary>
            /// Id
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// 图片
            /// </summary>
            public string ImgUrl { get; set; }
            /// <summary>
            /// 名称
            /// </summary>
            public string Name { get; set; }
        }
        #endregion
    }
}