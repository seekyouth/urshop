﻿namespace Urs.Plugin.Api.Models.Agents
{
    public class MoPostInfo
    {
        public string RealName { get; set; }

        public string Phone { get; set; }
    }
}
