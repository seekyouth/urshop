﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Urs.Data.Domain.Common;
using Plugin.Api.Models.ShoppingCart;

namespace Plugin.Api.Models.User
{
    /// <summary>
    /// 用户信息
    /// </summary>
    public partial class MoUserInfo
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 用户编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 用户Guid
        /// </summary>
        public string Guid { get; set; }
        /// <summary>
        /// 用户昵称
        /// </summary>
        public string Nickname { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public string Gender { get; set; }
        public int Points { get; set; }
        /// <summary>
        /// email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 头像Id
        /// </summary>
        public int AvatarPictureId { get; set; }
        /// <summary>
        /// 头像url
        /// </summary>
        public string AvatarUrl { get; set; }
        /// <summary>
        /// 生日
        /// </summary>
        public string BirthDate { get; set; }
        /// <summary>
        /// 公司
        /// </summary>
        public string Company { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string StreetAddress { get; set; }
        /// <summary>
        /// 邮编
        /// </summary>
        public string Zip { get; set; }
        /// <summary>
        /// 省份Id
        /// </summary>
        public string ProvinceName { get; set; }
        /// <summary>
        /// 城市Id
        /// </summary>
        public string CityName { get; set; }
        /// <summary>
        /// 区域Id
        /// </summary>
        public string AreaName { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 传真
        /// </summary>
        public string Fax { get; set; }

    }
}