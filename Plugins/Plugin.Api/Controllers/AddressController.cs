﻿
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Urs.Core;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Configuration;
using Plugin.Api.Extensions;
using Plugin.Api.Infrastructure;
using Plugin.Api.Models.UserAddress;
using Urs.Services.Common;
using Urs.Services.Users;
using Urs.Services.Localization;
using Urs.Framework.Controllers;

namespace Plugin.Api.Controllers
{
    /// <summary>
    /// 地址
    /// </summary>
    [ApiAuthorize]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/address")]
    [ApiController]
    public class AddressController : BaseApiController
    {
        private readonly ILocalizationService _localizationService;
        private readonly UserSettings _userSettings;
        private readonly AddressSettings _addressSettings;
        private readonly IUserService _userService;
        private readonly IAddressService _addressService;
        /// <summary>
        /// 构造器
        /// </summary>
        public AddressController(ILocalizationService localizationService,
            UserSettings userSettings,
            AddressSettings addressSettings,
            IUserService userService,
            IAddressService addressService)
        {
            this._localizationService = localizationService;
            this._userSettings = userSettings;
            this._addressSettings = addressSettings;
            this._userService = userService;
            this._addressService = addressService;
        }

        #region Utilities

        #endregion
        [HttpGet("list")]
        [ProducesResponseType(typeof(List<MoAddress>), 200)]
        public async Task<ApiResponse<List<MoAddress>>> List()
        {
            var user = RegisterUser;
            if (user == null)
                return ApiResponse<List<MoAddress>>.Warn("用户不存在");

            var model = new List<MoAddress>();

            var shipAddress = user.ShippingAddress;
            foreach (var address in user.Addresses)
            {
                var addressModel = new MoAddress();
                addressModel.PrepareModel(address, false, _addressSettings);
                if (shipAddress != null && shipAddress.Id == address.Id)
                    addressModel.Default = true;

                model.Add(addressModel);
            }
            return ApiResponse<List<MoAddress>>.Success(model);
        }
        [HttpPost("delete")]
        public ApiResponse Delete(int addressId)
        {
            var user = RegisterUser;
            if (user == null)
                return ApiResponse<List<MoAddress>>.Warn("用户不存在");

            var address = user.Addresses.Where(a => a.Id == addressId).FirstOrDefault();
            if (address != null)
            {
                user.RemoveAddress(address);
                _userService.UpdateUser(user);
                _addressService.DeleteAddress(address);

                return ApiResponse.Success();
            }
            return ApiResponse.NotFound();
        }
        [HttpPost("add")]
        public ApiResponse AddForm(MoAddressEdit model)
        {
            var user = RegisterUser;
            if (user == null)
                return ApiResponse<List<MoAddressEdit>>.Warn("用户不存在");

            if (ModelState.IsValid)
            {
                var address = model.ToEntity();
                address.CreateTime = DateTime.Now;
                _addressService.InsertAddress(address);

                user.UserAddressMappings.Add(new UserAddressMapping() { Address = address });
                _userService.UpdateUser(user);
                return ApiResponse.Success();
            }
            return ApiResponse.Warn(ModelState.FirstMessage());
        }

        [HttpPost("default")]
        public ApiResponse Default(int addressId)
        {
            var user = RegisterUser;
            var address = user.Addresses.Where(a => a.Id == addressId).FirstOrDefault();
            if (address == null)
                return ApiResponse.NotFound();

            user.ShippingAddressId = addressId;
            _userService.UpdateUser(user);

            return ApiResponse.Success();
        }

        [HttpPost("edit")]
        public ApiResponse EditForm(int addressId, MoAddressEdit model)
        {
            var user = RegisterUser;

            var address = user.Addresses.Where(a => a.Id == addressId).FirstOrDefault();
            if (address == null)
                return ApiResponse.NotFound();

            if (ModelState.IsValid)
            {
                address = model.ToEntity(address);
                address.Id = addressId;
                _addressService.UpdateAddress(address);

                return ApiResponse.Success();
            }
            return ApiResponse.Warn(ModelState.FirstMessage());
        }

    }
}
