﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using Urs.Admin.Validators.Topics;
using Urs.Framework;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Topics
{
    [Validator(typeof(TopicValidator))]
    public partial class TopicModel : BaseEntityModel
    {

        [UrsDisplayName("Admin.ContentManagement.Topics.Fields.SystemName")]
        
        public string SystemName { get; set; }
        [UrsDisplayName("Admin.ContentManagement.Topics.Fields.IncludeInSitemap")]
        public bool IncludeInSitemap { get; set; }
        [UrsDisplayName("Admin.ContentManagement.Topics.Fields.Published")]
        public bool Published { get; set; }
        [UrsDisplayName("Admin.ContentManagement.Topics.Fields.TopicStatusId")]
        public int TopicStatusId { get; set; }
        [UrsDisplayName("Admin.ContentManagement.Topics.Fields.Short")]
        public string Short { get; set; }
        [UrsDisplayName("Admin.ContentManagement.Topics.Fields.URL")]
        
        public string Url { get; set; }

        [UrsDisplayName("Admin.ContentManagement.Topics.Fields.Title")]
        
        public string Title { get; set; }

        [UrsDisplayName("Admin.ContentManagement.Topics.Fields.Body")]
        
        public string Body { get; set; }

        [UrsDisplayName("Admin.ContentManagement.Topics.Fields.MetaKeywords")]
        
        public string MetaKeywords { get; set; }

        [UrsDisplayName("Admin.ContentManagement.Topics.Fields.MetaDescription")]
        
        public string MetaDescription { get; set; }

        [UrsDisplayName("Admin.ContentManagement.Topics.Fields.MetaTitle")]
        
        public string MetaTitle { get; set; }
    }
}