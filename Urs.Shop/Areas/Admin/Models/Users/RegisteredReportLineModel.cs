﻿using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Users
{
    public partial class RegisteredReportLineModel : BaseModel
    {
        [UrsDisplayName("Admin.Users.Reports.RegisteredUsers.Fields.Period")]
        public string Period { get; set; }

        [UrsDisplayName("Admin.Users.Reports.RegisteredUsers.Fields.Users")]
        public int Users { get; set; }
    }
}