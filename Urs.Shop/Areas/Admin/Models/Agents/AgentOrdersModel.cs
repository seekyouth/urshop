﻿using System;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Agents
{
    public partial class AgentOrdersModel : BaseEntityModel
    {
        /// <summary>
        /// 订单Id
        /// </summary>
        public int OrderId { get; set; }
        public string OrderCode { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string AvatarUrl { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; set; }
        /// <summary>
        /// 订单金额
        /// </summary>
        public string OrderTotal { get; set; }
        /// <summary>
        /// 订单状态
        /// </summary>
        public string OrderStatus { get; set; }
        /// <summary>
        /// 订单状态Id
        /// </summary>
        public int OrderStatusId { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 订单创建时间
        /// </summary>
        public DateTime CreatedTime { get; set; }
    }
}