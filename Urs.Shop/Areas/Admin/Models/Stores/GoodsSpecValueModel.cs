﻿using FluentValidation.Attributes;
using Urs.Admin.Validators.Stores;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Stores
{
    [Validator(typeof(GoodsSpecValueValidator))]
    public partial class GoodsSpecValueModel : BaseEntityModel
    {
        public int GoodsSpecId { get; set; }

        public string Name { get; set; }
    }
}
