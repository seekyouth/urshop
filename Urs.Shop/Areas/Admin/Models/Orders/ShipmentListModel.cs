﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Orders
{
    public partial class ShipmentListModel : BaseModel
    {
        [UrsDisplayName("Admin.Orders.Shipments.List.StartDate")]
        [UIHint("DateNullable")]
        public DateTime? StartDate { get; set; }

        [UrsDisplayName("Admin.Orders.Shipments.List.EndDate")]
        [UIHint("DateNullable")]
        public DateTime? EndDate { get; set; }

        [UrsDisplayName("Admin.Orders.Shipments.List.ExpressName")]
        public string ExpressName { get; set; }

        [UrsDisplayName("Admin.Orders.Shipments.List.TrackingNumber")]
        
        public string TrackingNumber { get; set; }

        public bool DisplayPdfPackagingSlip { get; set; }
    }
}