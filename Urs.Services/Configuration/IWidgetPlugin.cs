using Microsoft.AspNetCore.Routing;
using System.Collections.Generic;
using Urs.Core.Plugins;

namespace Urs.Services.Configuration
{
    public partial interface IWidgetPlugin : IPlugin
    {
        IList<string> GetWidgetZones();
        string GetWidgetViewComponentName(string widgetZone);
        
    }
}
