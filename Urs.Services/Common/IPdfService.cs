using System.Collections.Generic;
using System.IO;
using Urs.Data.Domain.Stores;
using Urs.Data.Domain.Localization;
using Urs.Data.Domain.Orders;
using Urs.Data.Domain.Shipping;

namespace Urs.Services.Common
{
    public partial interface IPdfService
    {
        void PrintOrdersToPdf(Stream stream, IList<Order> orders);

        void PrintPackagingSlipsToPdf(Stream stream, IList<Shipment> shipments);

        
        void PrintGoodssToPdf(Stream stream, IList<Goods> list);
    }
}