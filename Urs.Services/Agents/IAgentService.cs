using System;
using Urs.Core;
using Urs.Data.Domain.Agents;

namespace Urs.Services.Agents
{
    /// <summary>
    /// agent service interface
    /// </summary>
    public partial interface IAgentService
    {
        #region AgentBonus

        /// <summary>
        /// ɾ��
        /// </summary>
        /// <param name="bonus">AgentBonus</param>
        void DeleteAgentBonus(AgentBonus bonus);
        /// <summary>
        /// Gets a bonusdd
        /// </summary>
        /// <param name="bonusId">The bonus identifier</param>
        /// <returns>AgentBonus</returns>
        AgentBonus GetAgentBonusById(int bonusId);
        AgentBonus GetAgentBonusByOrderId(int orderId);
        IPagedList<AgentBonus> GetList(int agentId = 0, int parentAgentId = 0, DateTime? startTime = null, DateTime? endTime = null,
            int pageIndex = 0, int pageSize = int.MaxValue);

        decimal GetFees(int agentId, bool? cash, bool? valid = null, DateTime? startTime = null, DateTime? endTime = null);
        int GetCount(int agentId);
        void ModifyCashForFees(int agentId);
        decimal GetParentFees(int agentId, bool? cash, bool? valid = null, DateTime? startTime = null, DateTime? endTime = null);
        /// <summary>
        /// Inserts a bonus
        /// </summary>
        /// <param name="bonus">AgentBonus</param>
        void InsertAgentBonus(AgentBonus bonus);

        /// <summary>
        /// Updates the bonus
        /// </summary>
        /// <param name="bonus">AgentBonus</param>
        void UpdateAgentBonus(AgentBonus bonus);

        #endregion


        #region AgentUser

        void DeleteAgentUser(AgentUser user);
        AgentUser GetAgentUserByUserId(int userId);
        IPagedList<AgentUser> GetUserList(int pageIndex = 0, int pageSize = int.MaxValue);
        void InsertAgentUser(AgentUser user);
        void UpdateAgentUser(AgentUser user);
        AgentSummary GetAgentSummary();
        AgentUserSummary GetAgentUserSummary(int agentId);
        #endregion
    }
}
