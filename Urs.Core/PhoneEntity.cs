﻿namespace Urs.Core
{
    public class PhoneEntity
    {
        public string phone { get; set; }
        public string guid { get; set; }
        public string openId { get; set; }
    }
}
